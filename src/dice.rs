use rand::Rng;

pub(crate) fn d4() -> u64 {
    rand::thread_rng().gen_range(1..=4)
}

pub(crate) fn d6() -> u64 {
    rand::thread_rng().gen_range(1..=6)
}

pub(crate) fn d10() -> u64 {
    rand::thread_rng().gen_range(1..=10)
}

pub(crate) fn d12() -> u64 {
    rand::thread_rng().gen_range(1..=12)
}

pub(crate) fn d100() -> u64 {
    rand::thread_rng().gen_range(1..=100)
}

pub(crate) fn die(sides: u64) -> u64 {
    rand::thread_rng().gen_range(1..=sides)
}
