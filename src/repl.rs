use crate::generator::CharacterGenerator;
use rustyline::error::ReadlineError;
use rustyline::DefaultEditor;

pub(crate) struct Repl<'a> {
    editor: DefaultEditor,
    generator: &'a CharacterGenerator<'a>,
}

impl Repl<'_> {
    pub(crate) fn new<'a>(generator: &'a CharacterGenerator) -> Repl<'a> {
        let editor = DefaultEditor::new();
        match editor {
            Ok(rl) => Repl {
                editor: rl,
                generator,
            },
            Err(err) => panic!("error: {:?}", err),
        }
    }

    pub(crate) fn run(&mut self) {
        loop {
            let readline = self.editor.readline(">> ");
            match readline {
                Ok(line) => {
                    println!("Line: {}", line);
                    if line == "new" {
                        let character = self.generator.generate("Porter Intorust", true);
                        println!("{:?}", character);
                    }
                }
                Err(ReadlineError::Interrupted) => {
                    println!("CTRL-C");
                    break;
                }
                Err(ReadlineError::Eof) => {
                    println!("CTRL-D");
                    break;
                }
                Err(err) => {
                    println!("Error: {:?}", err);
                    break;
                }
            }
        }
    }
}
