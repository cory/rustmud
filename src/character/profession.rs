use crate::character::attribute::AttributeType;
use crate::character::drawback::Drawback;
use crate::character::skill::Skill;
use serde::{Deserialize, Serialize};

#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq, Serialize, Deserialize)]
pub(crate) enum Archetype {
    Academic,
    Commoner,
    Knave,
    Ranger,
    Socialite,
    Warrior,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ProfessionTrait {
    pub(crate) name: String,
    pub(crate) description: String,
    pub(crate) effect: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub(crate) enum Tier {
    Basic,
    Intermediate,
    Advanced,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub(crate) struct SpecialTrait {
    pub(crate) name: String,
    pub(crate) description: String,
    pub(crate) effect: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Profession {
    pub(crate) archetype: Archetype,
    pub(crate) name: String,
    pub(crate) description: String,
    pub(crate) profession_trait: ProfessionTrait,
    pub(crate) reward_point_cost: u64,
    pub(crate) tier: Tier,
    pub(crate) special_trait: Option<SpecialTrait>,
    pub(crate) drawback: Option<Drawback>,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct SkillRank {
    skill: Skill,
    reward_point_cost: u64,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct BonusAdvancement {
    attribute_type: AttributeType,
    reward_point_cost: u64,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Talent {
    name: String,
    description: String,
    reward_point_cost: u64,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Advancement {
    pub(crate) professions: Vec<Profession>,
    pub(crate) skill_ranks: Vec<SkillRank>,
    pub(crate) bonus_advancements: Vec<BonusAdvancement>,
    pub(crate) talents: Vec<Talent>,
}
