use serde::{Serialize, Deserialize};
use crate::item::WeaponSkill;

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Parry {
    pub(crate) value: u64,
    pub(crate) skill: WeaponSkill,
}
