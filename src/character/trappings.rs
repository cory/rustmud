use crate::item::Item;
use serde::{Deserialize, Serialize};

const PENNIES_PER_SHILLING: u64 = 12;
const SHILLINGS_PER_CROWN: u64 = 20;

#[derive(Debug, Serialize, Deserialize)]
pub struct Money {
    pub(crate) brass_pennies: u64,
    pub(crate) silver_shillings: u64,
    pub(crate) gold_crowns: u64,
}

impl Money {
    pub(crate) fn add_brass_pennies(&self, pennies: u64) -> Money {
        let brass_pennies = self.as_brass_pennies() + pennies;
        Money::from_brass_pennies(brass_pennies)
    }

    pub(crate) fn as_brass_pennies(&self) -> u64 {
        self.brass_pennies
            + self.silver_shillings * PENNIES_PER_SHILLING
            + self.gold_crowns * SHILLINGS_PER_CROWN * PENNIES_PER_SHILLING
    }

    pub(crate) fn from_brass_pennies(brass_pennies: u64) -> Money {
        let gold_crowns = brass_pennies / (SHILLINGS_PER_CROWN * PENNIES_PER_SHILLING);
        let silver_shillings = (brass_pennies
            - gold_crowns * SHILLINGS_PER_CROWN * PENNIES_PER_SHILLING)
            / PENNIES_PER_SHILLING;
        let brass_pennies = brass_pennies
            - gold_crowns * SHILLINGS_PER_CROWN * PENNIES_PER_SHILLING
            - silver_shillings * PENNIES_PER_SHILLING;
        Money {
            brass_pennies,
            silver_shillings,
            gold_crowns,
        }
    }

    pub(crate) fn as_silver_shillings(&self) -> u64 {
        self.silver_shillings + self.gold_crowns * SHILLINGS_PER_CROWN
    }

    pub(crate) fn from_silver_shillings(silver_shillings: u64) -> Money {
        let gold_crowns = silver_shillings / SHILLINGS_PER_CROWN;
        let silver_shillings = silver_shillings - gold_crowns * SHILLINGS_PER_CROWN;
        Money {
            brass_pennies: 0,
            silver_shillings,
            gold_crowns,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Inventory {
    pub(crate) items: Vec<Item>,
}
