use crate::character::attribute::AttributeType;
use serde::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Skill {
    name: String,
    description: String,
    primary_attribute: AttributeType,
    // TODO: effect
}

// TODO: read these in from the file system
/*
// Combat
const MARTIAL_MELEE_SKILL: Skill = Skill {
    name: "Martial Melee".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Combat,
};

const MARTIAL_RANGED_SKILL: Skill = Skill {
    name: "Martial Ranged".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Combat,
};

const SIMPLE_MELEE_SKILL: Skill = Skill {
    name: "Simple Melee".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Combat,
};

const SIMPLE_RANGED_SKILL: Skill = Skill {
    name: "Simple Ranged".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Combat,
};

// Brawn
const ATHLETICS_SKILL: Skill = Skill {
    name: "Athletics".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Brawn,
};

const DRIVE_SKILL: Skill = Skill {
    name: "Drive".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Brawn,
};

const INTIMIDATE_SKILL: Skill = Skill {
    name: "Intimidate".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Brawn,
};

const TOUGHNESS_SKILL: Skill = Skill {
    name: "Toughness".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Brawn,
};

// Agility

const COORDINATION_SKILL: Skill = Skill {
    name: "Coordination".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Agility,
};

const PILOT_SKILL: Skill = Skill {
    name: "Pilot".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Agility,
};

const RIDE_SKILL: Skill = Skill {
    name: "Ride".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Agility,
};

const SKULLDUGGERY_SKILL: Skill = Skill {
    name: "Skullduggery".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Agility,
};

const STEALTH_SKILL: Skill = Skill {
    name: "Stealth".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Agility,
};

// Perception
const AWARENESS_SKILL: Skill = Skill {
    name: "Awareness".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Perception,
};
const EAVESDROP_SKILL: Skill = Skill {
    name: "Eavesdrop".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Perception,
};
const Scrutinze_SKILL: Skill = Skill {
    name: "Scrutinize".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Perception,
};
const SURVIVAL_SKILL: Skill = Skill {
    name: "Survival".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Perception,
};

// Intelligence
const ALCHEMY_SKILL: Skill = Skill {
    name: "Alchemy".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Intelligence,
};
const COUNTERFEIT_SKILL: Skill = Skill {
    name: "Counterfeit".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Intelligence,
};
const EDUCATION_SKILL: Skill = Skill {
    name: "Education".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Intelligence,
};
const FOLKLORE_SKILL: Skill = Skill {
    name: "Folklore".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Intelligence,
};
const GAMBLE_SKILL: Skill = Skill {
    name: "Gamble".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Intelligence,
};
const HEAL_SKILL: Skill = Skill {
    name: "Heal".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Intelligence,
};
const NAVIGATION_SKILL: Skill = Skill {
    name: "Navigation".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Intelligence,
};
const WARFARE_SKILL: Skill = Skill {
    name: "Warfare".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Intelligence,
};

// Willpower
const INCANTATION_SKILL: Skill = Skill {
    name: "Incantation".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Willpower,
};
const INTERROGATION_SKILL: Skill = Skill {
    name: "Interrogation".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Willpower,
};
const RESOLVE_SKILL: Skill = Skill {
    name: "Resolve".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Willpower,
};
const TRADECRAFT_SKILL: Skill = Skill {
    name: "Tradecraft".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Willpower,
};

// Fellowship
const BARGAIN_SKILL: Skill = Skill {
    name: "Bargain".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Fellowship,
};
const CHARM_SKILL: Skill = Skill {
    name: "Charm".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Fellowship,
};
const DISGUISE_SKILL: Skill = Skill {
    name: "Disguise".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Fellowship,
};
const GUILE_SKILL: Skill = Skill {
    name: "Guile".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Fellowship,
};
const HANDLE_ANIMAL_SKILL: Skill = Skill {
    name: "Handle Animal".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Fellowship,
};
const LEADERSHIP_SKILL: Skill = Skill {
    name: "Leadership".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Fellowship,
};
const RUMOR_SKILL: Skill = Skill {
    name: "Rumor".parse().unwrap(),
    description: "".to_string(),
    primary_attribute: Fellowship,
};
*/
