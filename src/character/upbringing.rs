use serde::{Serialize, Deserialize};
use crate::character::attribute::AttributeType;

#[derive(Debug, Serialize, Deserialize)]
pub struct Upbringing {
    pub(crate) name: String,
    pub(crate) favored_attribute: AttributeType,
}
