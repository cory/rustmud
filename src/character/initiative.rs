use serde::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Initiative {
    pub(crate) modifier: u64,
    pub(crate) three_plus_perception_bonus: u64,
    pub(crate) overage: u64,
    pub(crate) current: u64,
}
