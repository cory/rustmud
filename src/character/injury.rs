use serde::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize)]
pub enum Severity {
    Moderate,
    Serious,
    Grievous,
}

#[derive(Debug)]
pub struct Injury {
    name: String,
    description: String,
    severity: Severity,
    days_to_recuperate: u32,
    days_recuperated: u32,
    modified: u32,
    // TODO: effect
}
