use serde::{Deserialize, Serialize};

#[derive(Clone, Eq, PartialEq, Debug, Serialize, Deserialize)]
pub(crate) struct Drawback {
    pub(crate) name: String,
    pub(crate) description: String,
    pub(crate) effect: String,
}
