use serde::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct Encumbrance {
    pub(crate) modifier: u64,
    pub(crate) three_plus_brawn_bonus: u64,
    pub(crate) overage: u64,
    pub(crate) current: u64,
}
