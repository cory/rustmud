use serde::{Deserialize, Serialize};
use std::str::FromStr;

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq, Serialize, Deserialize)]
pub(crate) enum Ancestry {
    Human,
    Dwarf,
    Elf,
    Gnome,
    Halfling,
    Ogre,
}

impl FromStr for Ancestry {
    type Err = ();

    fn from_str(input: &str) -> Result<Ancestry, Self::Err> {
        match input {
            "Human" => Ok(Ancestry::Human),
            "Dwarf" => Ok(Ancestry::Dwarf),
            "Elf" => Ok(Ancestry::Elf),
            "Gnome" => Ok(Ancestry::Gnome),
            "Halfling" => Ok(Ancestry::Halfling),
            "Ogre" => Ok(Ancestry::Ogre),
            _ => Err(()),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct AncestralTrait {
    pub(crate) name: String,
    pub(crate) description: String,
    pub(crate) effect: String,
}

#[derive(Debug)]
pub(crate) enum SocialClass {
    Lowborn,
    Burgher,
    Aristocrat,
}

#[derive(Debug)]
pub(crate) enum AgeGroup {
    Young,
    Adult,
    MiddleAged,
    Elderly,
}

#[derive(Debug)]
pub(crate) enum Complexion {
    Pale,
    Fair,
    Light,
    LightTan,
    Tan,
    DarkTan,
    LightBrown,
    Brown,
    DarkBrown,
    Ebony,
}

#[derive(Debug)]
pub(crate) enum BuildType {
    Frail,
    Slender,
    Normal,
    Husky,
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub(crate) enum SeasonOfBirth {
    Spring,
    Summer,
    Autumn,
    Winter,
}

#[derive(Debug)]
pub(crate) enum HairColor {
    HoneyBrown,
    GoldenBrown,
    MediumBrown,
    DarkBrown,
    LightBrown,
    StrawYellow,
    GoldenYellow,
    Grey,
    Chestnut,
    Copper,
    Ginger,
    AshBlonde,
    SaltAndPepper,
    Sienna,
    Silver,
    SmokeGrey,
    White,
    IvoryWhite,
    BlueBlack,
    JetBlack,
    Red,
    FieryRed,
    Black,
}

#[derive(Debug)]
pub(crate) enum EyeColor {
    LightBrown,
    MediumBrown,
    DarkBrown,
    Hazel,
    Grey,
    PaleGreen,
    Copper,
    Green,
    Black,
    GreyBlue,
    Blue,
    DarkGreen,
    IceBlue,
    Violet,
    Amber,
    Silver,
    Honey,
    Emerald,
    Molasses,
    CrystalBlue,
    Sapphire,
    BluePurple,
    Gold,
    SilverGreen,
    Pink,
    Ruby,
    GreenWithingGreen,
}
