use serde::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize)]
pub(crate) enum AttributeType {
    Combat,
    Brawn,
    Agility,
    Perception,
    Intelligence,
    Willpower,
    Fellowship,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Attribute {
    pub(crate) attribute_type: AttributeType,
    pub(crate) score: u64,
    pub(crate) bonus: u64,
    pub(crate) bonus_advance: u64,
    pub(crate) favored: bool,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Attributes {
    pub(crate) combat: Attribute,
    pub(crate) brawn: Attribute,
    pub(crate) agility: Attribute,
    pub(crate) perception: Attribute,
    pub(crate) intelligence: Attribute,
    pub(crate) willpower: Attribute,
    pub(crate) fellowship: Attribute,
}
