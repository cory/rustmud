use serde::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize)]
pub enum DamageCondition {
    Unharmed,
    LightlyWounded,
    ModeratelyWounded,
    SeriouslyWounded,
    GrievouslyWounded,
    Slain,
}
