use serde::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct Alignment {
    pub(crate) order: String,
    pub(crate) chaos: String,
}
