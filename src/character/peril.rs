use serde::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize)]
pub enum PerilCondition {
    Unhindered,
    Imperiled,
    IgnoreOneSkillRank,
    IgnoreTwoSkillRanks,
    IgnoreThreeSkillRanks,
    Incapacitated,
}

#[derive(Debug, Serialize, Deserialize)]
struct Peril {
    threshold: u64,
    modifier: u64,
    modifier_plus_six: u64,
    modifier_plus_twelve: u64,
    modifier_plus_eighteen: u64,
}
