use crate::character::alignment::Alignment;
use crate::character::attribute::Attributes;
use crate::character::background::{
    AgeGroup, AncestralTrait, Ancestry, Complexion, EyeColor, HairColor, SeasonOfBirth, SocialClass,
};
use crate::character::drawback::Drawback;
use crate::character::encumbrance::Encumbrance;
use crate::character::initiative::Initiative;
use crate::character::injury::Injury;
use crate::character::movement::Movement;
use crate::character::parry::Parry;
use crate::character::profession::{Advancement, Archetype, Profession};
use crate::character::skill::Skill;
use crate::character::trappings::{Inventory, Money};
use crate::character::upbringing::Upbringing;
use crate::item::{Armor, Shield, Weapon};

pub mod alignment;
pub(crate) mod attribute;
pub(crate) mod background;
pub mod damage;
pub(crate) mod drawback;
pub(crate) mod encumbrance;
pub(crate) mod initiative;
pub(crate) mod injury;
pub(crate) mod movement;
pub(crate) mod parry;
pub mod peril;
pub mod profession;
pub mod skill;
pub(crate) mod trappings;
pub mod upbringing;

#[derive(Debug)]
pub(crate) struct Character<'a> {
    pub(crate) name: String,
    pub(crate) ancestry: Ancestry,
    pub(crate) ancestral_trait: &'a AncestralTrait,
    pub(crate) attributes: Attributes,
    pub(crate) social_class: SocialClass,
    pub(crate) age_group: AgeGroup,
    pub(crate) archetype: Archetype,
    pub(crate) profession: &'a Profession,
    pub(crate) distinguishing_marks: Vec<String>,
    pub(crate) complexion: Complexion,
    pub(crate) upbringing: Upbringing,
    pub(crate) alignment: Alignment,
    pub(crate) drawbacks: Vec<Drawback>,
    pub(crate) season_of_birth: SeasonOfBirth,
    pub(crate) dooming: String,
    pub(crate) fate_points: u64,
    pub(crate) reputation_points: u64,
    pub(crate) height_inches: u64,
    pub(crate) weight_pounds: u64,
    pub(crate) hair_color: HairColor,
    pub(crate) eye_color: EyeColor,
    pub(crate) skills: Vec<Skill>,
    pub(crate) armor: Armor,
    pub(crate) shield: Shield,
    pub(crate) weapons: Vec<Weapon>,
    pub(crate) initiative: Initiative,
    pub(crate) movement: Movement,
    pub(crate) dodge: u32,
    pub(crate) parry: Parry,
    pub(crate) injuries: Vec<Injury>,
    pub(crate) money: Money,
    pub(crate) encumbrance: Encumbrance,
    pub(crate) inventory: Inventory,
    pub(crate) advancement: Advancement,
}
