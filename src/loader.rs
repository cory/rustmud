use std::collections::HashMap;
use std::fs;
use std::path::Path;

use serde::{Deserialize, Serialize};
use serde_yaml::{self};

use crate::character::background::{AncestralTrait, Ancestry, SeasonOfBirth};
use crate::character::drawback::Drawback;
use crate::character::profession::{Archetype, Profession};
use crate::config::Config;

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct AncestryDefinition {
    pub(crate) ancestry: Ancestry,
    pub(crate) traits: Vec<AncestralTrait>,
    pub(crate) population_percentage: u64,
}

pub(crate) struct Loader<'a> {
    pub(crate) config: &'a Config,
}

impl Loader<'_> {
    pub(crate) fn load_ancestries(&self) -> HashMap<Ancestry, AncestryDefinition> {
        let mut map: HashMap<Ancestry, AncestryDefinition> = HashMap::new();
        let mut ancestry_config_path = self.config.path.to_string();
        ancestry_config_path.push_str("/assets/ancestry");
        let path = Path::new(ancestry_config_path.as_str());
        if path.is_dir() {
            for result in fs::read_dir(path).unwrap() {
                let entry = result.unwrap();
                let ancestry_yaml_path = entry.path();
                println!("loading ancestry from {}", ancestry_yaml_path.display());
                let ancestry_yaml =
                    fs::read_to_string(ancestry_yaml_path).expect("failed to read ancestry file");
                let definition: AncestryDefinition =
                    serde_yaml::from_str(ancestry_yaml.as_str()).unwrap();
                let ancestry = definition.ancestry;
                map.insert(ancestry, definition);
            }
        }
        map
    }

    pub(crate) fn load_professions(&self) -> HashMap<Archetype, Vec<Profession>> {
        let mut map: HashMap<Archetype, Vec<Profession>> = HashMap::new();

        let mut profession_config_path = self.config.path.to_string();
        profession_config_path.push_str("/assets/profession");
        let path = Path::new(profession_config_path.as_str());
        if path.is_dir() {
            for result in fs::read_dir(path).unwrap() {
                let entry = result.unwrap();
                let profession_yaml_path = entry.path();
                println!("loading profession from {}", profession_yaml_path.display());
                let profession_yaml = fs::read_to_string(profession_yaml_path)
                    .expect("failed to read profession file");
                let profession: Profession =
                    serde_yaml::from_str(profession_yaml.as_str()).unwrap();
                let archetype = profession.archetype;
                let professions = map.get_mut(&profession.archetype);
                match professions {
                    Some(professions) => {
                        professions.push(profession);
                    }
                    None => {
                        let professions: Vec<Profession> = vec![profession];
                        map.insert(archetype, professions);
                    }
                }
            }
        }
        map
    }

    pub(crate) fn load_distinguishing_marks(&self) -> Vec<String> {
        let mut distinguishing_marks_config_path = self.config.path.to_string();
        distinguishing_marks_config_path.push_str("/assets/appearance/distinguishing_marks.yaml");
        let distinguishing_marks_yaml = fs::read_to_string(distinguishing_marks_config_path)
            .expect("failed to read distinguishing marks file");
        let marks: Vec<String> = serde_yaml::from_str(distinguishing_marks_yaml.as_str()).unwrap();
        marks
    }

    pub(crate) fn load_doomings(&self) -> HashMap<SeasonOfBirth, Vec<String>> {
        let mut doomings_config_path = self.config.path.to_string();
        doomings_config_path.push_str("/assets/appearance/dooming.yaml");
        let doomings_yaml = fs::read_to_string(doomings_config_path)
            .expect("failed to read doomings file");
        let doomings: HashMap<SeasonOfBirth, Vec<String>> = serde_yaml::from_str(doomings_yaml.as_str()).unwrap();
        doomings
    }
    
    pub(crate) fn load_drawbacks(&self) -> Vec<Drawback> {
        let mut drawbacks_config_path = self.config.path.to_string();
        drawbacks_config_path.push_str("/assets/appearance/drawbacks.yaml");
        let drawbacks_yaml = fs::read_to_string(drawbacks_config_path)
            .expect("failed to read drawbacks file");
        let drawbacks: Vec<Drawback> = serde_yaml::from_str(drawbacks_yaml.as_str()).unwrap();
        drawbacks
    }
}
