use crate::config::{load_config, Config};
use crate::generator::CharacterGenerator;
use crate::loader::Loader;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::TcpListener;

pub mod character;
mod config;
mod dice;
mod generator;
pub mod item;
mod loader;
mod repl;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let config: Config = load_config();
    let loader: Loader = Loader { config: &config };
    let ancestries = loader.load_ancestries();
    let professions = loader.load_professions();
    let distinguishing_marks = loader.load_distinguishing_marks();
    let doomings = loader.load_doomings();
    let drawbacks = loader.load_drawbacks();
    let generator: CharacterGenerator = CharacterGenerator {
        ancestries: &ancestries,
        professions: &professions,
        distinguishing_marks: &distinguishing_marks,
        doomings: &doomings,
        drawbacks: &drawbacks,
    };

    let listener = TcpListener::bind("127.0.0.1:8080").await?;
    loop {
        let (mut socket, _) = listener.accept().await?;

        tokio::spawn(async move {
            let mut buf = [0; 1024];

            // Create the repl for this connection
            // let mut repl = repl::Repl::new(&generator);

            // Connection the repl to the socker so it can consume/produce messages
            // repl.run();

            // In a loop, read data from the socket and write the data back.
            loop {
                let n = match socket.read(&mut buf).await {
                    // socket closed
                    Ok(n) if n == 0 => return,
                    Ok(n) => n,
                    Err(e) => {
                        eprintln!("failed to read from socket; err = {:?}", e);
                        return;
                    }
                };

                // Write the data back
                if let Err(e) = socket.write_all(&buf[0..n]).await {
                    eprintln!("failed to write to socket; err = {:?}", e);
                    return;
                }
            }
        });
    }
}
