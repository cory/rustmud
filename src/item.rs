use serde::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct ArmorQuality {
    name: String,
    description: String,
    // TODO: effect
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Armor {
    pub(crate) name: String,
    pub(crate) damage_threshold_modifier: u64,
    pub(crate) encumbrance_value: u64,
    pub(crate) qualities: Vec<ArmorQuality>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ShieldQuality {
    name: String,
    description: String,
    // TODO: effect
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Shield {
    pub(crate) name: String,
    pub(crate) damage_threshold_modifier: u64,
    pub(crate) encumbrance_value: u64,
    pub(crate) qualities: Vec<ShieldQuality>,
}

#[derive(Debug, Serialize, Deserialize)]
pub enum WeaponSkill {
    SimpleMelee,
    SimpleRanged,
    MartialMelee,
    MartialRanged,
}

#[derive(Debug, Serialize, Deserialize)]
pub enum Handling {
    OneHanded,
    TwoHanded,
    OneOrTwoHanded,
}

#[derive(Debug, Serialize, Deserialize)]
pub enum WeaponDistance {
    Engaged,
    OneYard,
    Ranged,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct WeaponQuality {
    name: String,
    description: String,
    // TODO: effect
}

#[derive(Debug, Serialize, Deserialize)]
pub enum WeaponType {
    Bladed,
    Crushing,
    None,
    Missile,
    Gunpowder,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Weapon {
    name: String,
    skill: WeaponSkill,
    load: u32,
    handling: Handling,
    distance: WeaponDistance,
    qualities: Vec<WeaponQuality>,
    weapon_type: WeaponType,
    encumbrance_value: u64,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Item {
    name: String,
    description: String,
    encumbrance_value: u64,
}
