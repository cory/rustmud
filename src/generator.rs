use std::collections::HashMap;

use rand::Rng;

use crate::character::alignment::Alignment;
use crate::character::attribute::AttributeType::{
    Agility, Brawn, Combat, Fellowship, Intelligence, Perception, Willpower,
};
use crate::character::attribute::{Attribute, Attributes};
use crate::character::background::AgeGroup::{Adult, Elderly, MiddleAged, Young};
use crate::character::background::Complexion::{
    Brown, DarkBrown, DarkTan, Ebony, Fair, Light, LightBrown, LightTan, Pale, Tan,
};
use crate::character::background::EyeColor::Blue;
use crate::character::background::SeasonOfBirth::{Autumn, Spring, Summer, Winter};
use crate::character::background::SocialClass::{Aristocrat, Burgher, Lowborn};
use crate::character::background::{
    AgeGroup, AncestralTrait, Ancestry, Complexion, EyeColor, HairColor, SeasonOfBirth, SocialClass,
};
use crate::character::drawback::Drawback;
use crate::character::encumbrance::Encumbrance;
use crate::character::initiative::Initiative;
use crate::character::injury::Injury;
use crate::character::movement::Movement;
use crate::character::parry::Parry;
use crate::character::profession::Archetype::{
    Academic, Commoner, Knave, Ranger, Socialite, Warrior,
};
use crate::character::profession::{Advancement, Archetype, Profession};
use crate::character::skill::Skill;
use crate::character::trappings::{Inventory, Money};
use crate::character::upbringing::Upbringing;
use crate::character::Character;
use crate::dice::{d10, d100, d12, d4, d6, die};
use crate::item::{Armor, Shield, Weapon, WeaponSkill};
use crate::loader::AncestryDefinition;

pub(crate) struct CharacterGenerator<'a> {
    pub(crate) ancestries: &'a HashMap<Ancestry, AncestryDefinition>,
    pub(crate) professions: &'a HashMap<Archetype, Vec<Profession>>,
    pub(crate) distinguishing_marks: &'a Vec<String>,
    pub(crate) doomings: &'a HashMap<SeasonOfBirth, Vec<String>>,
    pub(crate) drawbacks: &'a Vec<Drawback>,
}

impl CharacterGenerator<'_> {
    pub(crate) fn generate(&self, name: &str, take_drawback: bool) -> Character {
        let ancestry = generate_ancestry(self.ancestries);
        let ancestral_trait = generate_ancestral_trait(self.ancestries, ancestry);
        let attributes = generate_attributes();
        let social_class = generate_social_class();
        let name = String::from(name);
        let age_group = generate_age_group();
        let distinguishing_marks =
            generate_distinguishing_marks(self.distinguishing_marks, &age_group);
        let archetype = generate_archetype();
        let profession = generate_profession(&archetype, self.professions);
        let complexion = generate_complexion();
        let upbringing = generate_upbringing();
        let alignment = generate_alignment();
        let drawback = generate_drawback(self.drawbacks);
        let drawbacks = vec![drawback];
        let season_of_birth = generate_season_of_birth();
        let dooming = generate_dooming(self.doomings, &season_of_birth);
        let fate_points = if take_drawback { 2 } else { 1 };
        let reputation_points = 0;
        let height_inches = generate_height();
        let weight_pounds = generate_weight();
        let hair_color = generate_hair_color();
        let eye_color = generate_eye_color();
        let skills = generate_skills();
        let armor = generate_armor();
        let shield = generate_shield();
        let weapons = generate_weapons();
        let initiative = generate_initiative(&attributes);
        let movement = generate_movement(&attributes);
        let dodge = 0;
        let parry = generate_parry();
        let injuries: Vec<Injury> = Vec::new();
        let money = generate_money(&social_class);
        let encumbrance = generate_encumbrance(&attributes);
        let inventory = generate_inventory(&archetype);
        let advancement = generate_advancement(profession);
        Character {
            name,
            ancestry,
            ancestral_trait,
            attributes,
            social_class,
            age_group,
            archetype,
            profession,
            distinguishing_marks,
            complexion,
            upbringing,
            alignment,
            drawbacks,
            season_of_birth,
            dooming,
            fate_points,
            reputation_points,
            height_inches,
            weight_pounds,
            hair_color,
            eye_color,
            skills,
            armor,
            shield,
            weapons,
            initiative,
            movement,
            dodge,
            parry,
            injuries,
            money,
            encumbrance,
            inventory,
            advancement,
        }
    }
}

struct RollRange {
    min: u64,
    max: u64,
}

fn generate_ancestry(ancestries: &HashMap<Ancestry, AncestryDefinition>) -> Ancestry {
    let mut roll_table: HashMap<&Ancestry, RollRange> = HashMap::new();
    let mut roll_range_limit: u64 = 0;
    for (ancestry, defn) in ancestries {
        let ancestry = ancestry;
        roll_table.insert(
            ancestry,
            RollRange {
                min: roll_range_limit,
                max: roll_range_limit + defn.population_percentage,
            },
        );
        roll_range_limit += defn.population_percentage
    }

    let roll: u64 = d100();
    for (ancestry, roll_range) in roll_table {
        if roll > roll_range.min && roll <= roll_range.max {
            return *ancestry;
        }
    }
    panic!("Failed to match an ancestry to roll");
}

fn generate_attribute_value() -> u64 {
    d10() + d10() + d10() + 25
}

fn generate_attributes() -> Attributes {
    let combat = generate_attribute_value();
    let brawn = generate_attribute_value();
    let agility = generate_attribute_value();
    let perception = generate_attribute_value();
    let intelligence = generate_attribute_value();
    let willpower = generate_attribute_value();
    let fellowship = generate_attribute_value();
    Attributes {
        combat: Attribute {
            attribute_type: Combat,
            score: combat,
            bonus: combat / 10,
            bonus_advance: 0,
            favored: false,
        },
        brawn: Attribute {
            attribute_type: Brawn,
            score: brawn,
            bonus: brawn / 10,
            bonus_advance: 0,
            favored: false,
        },
        agility: Attribute {
            attribute_type: Agility,
            score: agility,
            bonus: agility / 10,
            bonus_advance: 0,
            favored: false,
        },
        perception: Attribute {
            attribute_type: Perception,
            score: perception,
            bonus: perception / 10,
            bonus_advance: 0,
            favored: false,
        },
        intelligence: Attribute {
            attribute_type: Intelligence,
            score: intelligence,
            bonus: intelligence / 10,
            bonus_advance: 0,
            favored: false,
        },
        willpower: Attribute {
            attribute_type: Willpower,
            score: willpower,
            bonus: willpower / 10,
            bonus_advance: 0,
            favored: false,
        },
        fellowship: Attribute {
            attribute_type: Fellowship,
            score: fellowship,
            bonus: fellowship / 10,
            bonus_advance: 0,
            favored: false,
        },
    }
}

fn generate_ancestral_trait(
    ancestries: &HashMap<Ancestry, AncestryDefinition>,
    ancestry: Ancestry,
) -> &AncestralTrait {
    let roll = d12() as usize;
    let ancestry_definition = ancestries
        .get(&ancestry)
        .expect("No ancestry definition found");
    let traits = &ancestry_definition.traits;
    if roll > traits.len() {
        panic!("roll greater than ancestral trait count")
    }
    // println!("rolled {} for ancestral trait", roll);
    let ancestral_trait: Option<&AncestralTrait> = traits.get(roll - 1);
    match ancestral_trait {
        Some(ancestral_trait) => ancestral_trait,
        None => panic!("no ancestral trait found"),
    }
}

fn generate_social_class() -> SocialClass {
    let roll = d100();
    if roll < 80 {
        return Lowborn;
    } else if roll < 95 {
        return Burgher;
    }
    Aristocrat
}

fn generate_age_group() -> AgeGroup {
    let roll = d4();
    match roll {
        1 => Young,
        2 => Adult,
        3 => MiddleAged,
        4 => Elderly,
        _ => panic!("invalid value"),
    }
}

fn generate_distinguishing_marks(
    distinguishing_marks: &[String],
    age_group: &AgeGroup,
) -> Vec<String> {
    let mut marks: Vec<String> = Vec::new();
    match age_group {
        Young => {
            println!("no distinguishing marks for young");
        }
        Adult => {
            println!("1 distinguishing marks for adult");
            let roll = d100() as usize;
            let mark: Option<&String> = distinguishing_marks.get(roll);
            match mark {
                Some(mark) => marks.push(mark.to_string()),
                None => panic!("no mark found for roll {}", roll),
            }
        }
        MiddleAged => {
            println!("2 distinguishing marks for middleaged");
            for _ in 0..1 {
                let roll = d100() as usize;
                let mark: Option<&String> = distinguishing_marks.get(roll);
                match mark {
                    Some(mark) => marks.push(mark.to_string()),
                    None => panic!("no mark found for roll {}", roll),
                }
            }
        }
        Elderly => {
            println!("3 distinguishing marks for elderly");
            for _ in 0..2 {
                let roll = d100() as usize;
                let mark: Option<&String> = distinguishing_marks.get(roll);
                match mark {
                    Some(mark) => marks.push(mark.to_string()),
                    None => panic!("no mark found for roll {}", roll),
                }
            }
        }
    }
    marks
}

fn generate_archetype() -> Archetype {
    let roll = d6();
    match roll {
        1 => Academic,
        2 => Commoner,
        3 => Knave,
        4 => Ranger,
        5 => Socialite,
        6 => Warrior,
        _ => panic!("invalid value"),
    }
}

fn generate_profession<'a>(
    archetype: &Archetype,
    professions: &'a HashMap<Archetype, Vec<Profession>>,
) -> &'a Profession {
    let roll = d12() as usize;
    println!("rolled {} for profession", roll);
    let archetype_professions = professions
        .get(archetype)
        .expect("No archetype professions found");
    if roll > archetype_professions.len() {
        panic!(
            "roll {} greater than archetype profession count {}",
            roll,
            archetype_professions.len()
        )
    }
    archetype_professions.get(roll - 1).unwrap()
}

fn generate_complexion() -> Complexion {
    let roll = d10();
    match roll {
        1 => Pale,
        2 => Fair,
        3 => Light,
        4 => LightTan,
        5 => Tan,
        6 => DarkTan,
        7 => LightBrown,
        8 => DarkBrown,
        9 => Brown,
        10 => Ebony,
        _ => panic!("invalid value"),
    }
}

fn generate_upbringing() -> Upbringing {
    let roll = rand::thread_rng().gen_range(1..=7);
    match roll {
        1 => Upbringing {
            name: "Cultured".to_string(),
            favored_attribute: Fellowship,
        },
        2 => Upbringing {
            name: "Forgotten".to_string(),
            favored_attribute: Agility,
        },
        3 => Upbringing {
            name: "Industrious".to_string(),
            favored_attribute: Brawn,
        },
        4 => Upbringing {
            name: "Militant".to_string(),
            favored_attribute: Combat,
        },
        5 => Upbringing {
            name: "Opportunistic".to_string(),
            favored_attribute: Perception,
        },
        6 => Upbringing {
            name: "Reverent".to_string(),
            favored_attribute: Willpower,
        },
        7 => Upbringing {
            name: "Scholastic".to_string(),
            favored_attribute: Intelligence,
        },
        _ => panic!("invalid value"),
    }
}

fn generate_alignment() -> Alignment {
    let roll = rand::thread_rng().gen_range(1..=25);
    match roll {
        1 => Alignment {
            order: "Adaptation".to_string(),
            chaos: "Mayhem".to_string(),
        },
        2 => Alignment {
            order: "Ambition".to_string(),
            chaos: "Tyranny".to_string(),
        },
        3 => Alignment {
            order: "Candor".to_string(),
            chaos: "Cruelty".to_string(),
        },
        4 => Alignment {
            order: "Charity".to_string(),
            chaos: "Pity".to_string(),
        },
        5 => Alignment {
            order: "Compassion".to_string(),
            chaos: "Melancholy".to_string(),
        },
        6 => Alignment {
            order: "Cunning".to_string(),
            chaos: "Deceit".to_string(),
        },
        7 => Alignment {
            order: "Dignity".to_string(),
            chaos: "Vehemence".to_string(),
        },
        8 => Alignment {
            order: "Diplomacy".to_string(),
            chaos: "Hypocrisy".to_string(),
        },
        9 => Alignment {
            order: "Duty".to_string(),
            chaos: "Fatalism".to_string(),
        },
        10 => Alignment {
            order: "Enlightenment".to_string(),
            chaos: "Detachment".to_string(),
        },
        11 => Alignment {
            order: "Ferocity".to_string(),
            chaos: "Hatred".to_string(),
        },
        12 => Alignment {
            order: "Gentility".to_string(),
            chaos: "Cowardice".to_string(),
        },
        13 => Alignment {
            order: "Gravitas".to_string(),
            chaos: "Vanity".to_string(),
        },
        14 => Alignment {
            order: "Heroism".to_string(),
            chaos: "Martyrdom".to_string(),
        },
        15 => Alignment {
            order: "Humility".to_string(),
            chaos: "Incompetence".to_string(),
        },
        16 => Alignment {
            order: "Impiety".to_string(),
            chaos: "Heresy".to_string(),
        },
        17 => Alignment {
            order: "Independence".to_string(),
            chaos: "Rebellion".to_string(),
        },
        18 => Alignment {
            order: "Mystery".to_string(),
            chaos: "Exclusion".to_string(),
        },
        19 => Alignment {
            order: "Pride".to_string(),
            chaos: "Arrogance".to_string(),
        },
        20 => Alignment {
            order: "Romanticism".to_string(),
            chaos: "Lechery".to_string(),
        },
        21 => Alignment {
            order: "Skepticism".to_string(),
            chaos: "Cynicism".to_string(),
        },
        22 => Alignment {
            order: "Sophistication".to_string(),
            chaos: "Indulgence".to_string(),
        },
        23 => Alignment {
            order: "Wisdom".to_string(),
            chaos: "Rancor".to_string(),
        },
        24 => Alignment {
            order: "Wit".to_string(),
            chaos: "Scorn".to_string(),
        },
        25 => Alignment {
            order: "Zeal".to_string(),
            chaos: "Fanaticism".to_string(),
        },
        _ => panic!("invalid value"),
    }
}

fn generate_drawback(drawbacks: &Vec<Drawback>) -> Drawback {
    let roll = die(drawbacks.len() as u64) - 1;
    drawbacks.get(roll as usize).unwrap().clone()
}

fn generate_season_of_birth() -> SeasonOfBirth {
    let roll = d4();
    match roll {
        1 => Spring,
        2 => Summer,
        3 => Autumn,
        4 => Winter,
        _ => panic!("invalid value"),
    }
}

fn generate_dooming(
    doomings: &HashMap<SeasonOfBirth, Vec<String>>,
    season_of_birth: &SeasonOfBirth,
) -> String {
    // println!("fetching dooming for season {:?}", season_of_birth);
    // println!("from doomings {:?}", doomings);
    let season_doomings = doomings.get(season_of_birth);
    match season_doomings {
        Some(season_doomings) => {
            let roll = die(season_doomings.len() as u64) as usize;
            let dooming: Option<&String> = season_doomings.get(roll);
            match dooming {
                Some(dooming) => dooming.to_string(),
                None => panic!("no dooming found for roll {}", roll),
            }
        }
        None => panic!("no dooming found for season"),
    }
}

fn generate_height() -> u64 {
    70
}

fn generate_weight() -> u64 {
    170
}

fn generate_hair_color() -> HairColor {
    HairColor::LightBrown
}

fn generate_eye_color() -> EyeColor {
    Blue
}

fn generate_skills() -> Vec<Skill> {
    Vec::new()
}

fn generate_armor() -> Armor {
    Armor {
        name: "Clothing".to_string(),
        damage_threshold_modifier: 0,
        encumbrance_value: 0,
        qualities: Vec::new(),
    }
}

fn generate_weapons() -> Vec<Weapon> {
    Vec::new()
}

fn generate_shield() -> Shield {
    Shield {
        name: "None".to_string(),
        damage_threshold_modifier: 0,
        encumbrance_value: 0,
        qualities: Vec::new(),
    }
}

fn generate_initiative(attributes: &Attributes) -> Initiative {
    Initiative {
        modifier: 0,
        three_plus_perception_bonus: attributes.perception.bonus * 3,
        overage: 0,
        current: 0,
    }
}

fn generate_movement(attributes: &Attributes) -> Movement {
    Movement {
        modifier: 0,
        three_plus_agility_bonus: attributes.agility.bonus * 3,
        overage: 0,
        current: 0,
    }
}

fn generate_parry() -> Parry {
    Parry {
        value: 0,
        skill: WeaponSkill::SimpleMelee,
    }
}

fn generate_money(social_class: &SocialClass) -> Money {
    match social_class {
        Lowborn => Money {
            brass_pennies: d10() + d10() + d10() + 3,
            silver_shillings: 0,
            gold_crowns: 0,
        },
        Burgher => Money {
            brass_pennies: 0,
            silver_shillings: d10() + d10() + 2,
            gold_crowns: 0,
        },
        Aristocrat => Money {
            brass_pennies: 0,
            silver_shillings: 0,
            gold_crowns: d10() + 1,
        },
    }
}

fn generate_encumbrance(attributes: &Attributes) -> Encumbrance {
    Encumbrance {
        modifier: 0,
        three_plus_brawn_bonus: attributes.brawn.bonus * 3,
        overage: 0,
        current: 0,
    }
}

fn generate_inventory(archetype: &Archetype) -> Inventory {
    Inventory { items: Vec::new() }
}

fn generate_advancement(profession: &Profession) -> Advancement {
    Advancement {
        professions: Vec::new(),
        skill_ranks: Vec::new(),
        bonus_advancements: Vec::new(),
        talents: Vec::new(),
    }
}
