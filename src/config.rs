pub(crate) struct Config {
    pub(crate) path: String,
}

pub(crate) fn load_config() -> Config {
    return Config{ path: "config".to_string() }
}